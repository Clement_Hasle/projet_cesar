from tkinter.messagebox import *
from tkinter import * 
import string

listeAlphabetMin = string.ascii_lowercase
listeAlphabetMaj = string.ascii_uppercase

NBDECALAGEMAX = 100

#Evenement au clic de la zone de texte de saisie qui efface le contenu
def onClick(event):
    zoneTexteSaisie.delete(0, len(zoneTexteSaisie.get()))

#Evenement au clic de la zone de texte de saisie qui efface le contenu
def onClick2(event):
    zoneNbDecalage.delete(0, len(zoneNbDecalage.get()))

"""Fonction Chiffrage
    La fonction prend une phrase en paramètre et la crypte avec la clé de cryptage passée en paramètre également

    IN: Chaîne de caractères: phraseCodee, Entier: nbDecalage
    OUT: Chaîne de caractères : phraseDecodee
"""
def chiffrage(phraseDecodee, nbDecalage):
    phraseCodee = ''
    for caractere in phraseDecodee:
        phraseCodee = phraseCodee + chr(ord(caractere) + nbDecalage)
    return phraseCodee

"""Fonction dechiffrage
    La fonction prend une phrase en paramètre et la décrypte avec la clé de cryptage passée en paramètre également

    IN: Chaîne de caractères: phraseCodee, Entier: nbDecalage
    OUT: Chaîne de caractères : phraseDecodee
"""
def dechiffrage(phraseCodee, nbDecalage):
    phraseDecodee = ''
    for caractere in phraseCodee:
    
        phraseDecodee = phraseDecodee + chr(ord(caractere) - nbDecalage)
    return phraseDecodee

"""Fonction ChiffrageCirculaire
    La fonction prend une phrase en paramètre et la crypte de manière circulaire 
    avec la clé de cryptage passée en paramètre également

    IN: Chaîne de caractères: phraseDecodee, Entier: nbDecalage
    OUT: Chaîne de caractères : phraseCodee
"""
def chiffrageCirculaire(phraseDecodee, nbDecalage):
    phraseCodee = ''
    if(nbDecalage>26): 
        nbDecalage = nbDecalage % 26
    listeAlphabetMinDecale = listeAlphabetMin[nbDecalage:] + listeAlphabetMin[:nbDecalage]
    listeAlphabetMajDecale = listeAlphabetMaj[nbDecalage:] + listeAlphabetMaj[:nbDecalage]
    for caractere in phraseDecodee:
        if(caractere <= 'z' and caractere >= 'a'):
            phraseCodee = phraseCodee + listeAlphabetMinDecale[listeAlphabetMin.index(caractere)]
        elif(caractere <= 'Z' and caractere >= 'A'):
            phraseCodee = phraseCodee + listeAlphabetMajDecale[listeAlphabetMaj.index(caractere)]
        else:
            phraseCodee = phraseCodee + caractere
    return phraseCodee

"""Fonction dechiffrageCirculaire
    La fonction prend une phrase en paramètre et la décrypte de manière circulaire
    avec la clé de cryptage passée en paramètre également

    IN: Chaîne de caractères: phraseCodee, Entier: nbDecalage
    OUT: Chaîne de caractères : phraseDecodee
"""
def dechiffrageCirculaire(phraseCodee, nbDecalage):
    phraseDecodee = ''
    if(nbDecalage>26):
        nbDecalage = nbDecalage % 26
    listeAlphabetMinDecale = listeAlphabetMin[nbDecalage:] + listeAlphabetMin[:nbDecalage]
    listeAlphabetMajDecale = listeAlphabetMaj[nbDecalage:] + listeAlphabetMaj[:nbDecalage]
    for caractere in phraseCodee:
        if(caractere <= 'z' and caractere >= 'a'):
            phraseDecodee = phraseDecodee + listeAlphabetMin[listeAlphabetMinDecale.index(caractere)]
        elif(caractere <= 'Z' and caractere >= 'A'):
            phraseDecodee = phraseDecodee + listeAlphabetMaj[listeAlphabetMajDecale.index(caractere)]
        else:
            phraseDecodee = phraseDecodee + caractere
        
    return phraseDecodee

"""Fonction affichage
    La fonction choisi quelle fonction de cryptage ou decryptage appeler selon les choix de l'utilisateur
    et affiche la phrase cryptée ou décryptée correspondante dans le champ prévu à cet effet
    et affiche également des warning en cas de clef incorrecte
"""
def affichage():
    try:
        if int(zoneNbDecalage.get()) > 0 and int(zoneNbDecalage.get()) < NBDECALAGEMAX:
            nbDecalage = int(zoneNbDecalage.get())
            if valeurChoix.get() == 0:
                if valeurChoixCirculaire.get() == 0:
                    zoneAffichageResultat.config(text = chiffrage(phraseSaisie.get(), nbDecalage))
                else:
                    zoneAffichageResultat.config(text = chiffrageCirculaire(phraseSaisie.get(), nbDecalage))
            else:
                if valeurChoixCirculaire.get() == 0:
                    zoneAffichageResultat.config(text = dechiffrage(phraseSaisie.get(), nbDecalage))
                else:
                    zoneAffichageResultat.config(text = dechiffrageCirculaire(phraseSaisie.get(), nbDecalage))
        else:
            showwarning('zoneNbDecalage', 'Veuillez rentrer un nombre valide <%d' % NBDECALAGEMAX)
    except ValueError:
        showwarning('zoneNbDecalage', 'Veuillez rentrer un nombre')

if __name__=="__main__":
    root = Tk()
    root.title("Le Chiffre De César")
    root.rowconfigure(2, weight=1)
    root.columnconfigure(2, weight=1) 

    phraseSaisie = StringVar()
    phraseSaisie.set("Entrez le texte à coder")
    phraseNombre = StringVar()
    phraseNombre.set("Entrez la clé de cryptage")
    phraseChoixCirculaire = StringVar()
    phraseChoixCirculaire.set('Circulaire')
    valeurChoix = IntVar()
    valeurChoixCirculaire = IntVar()

    #Radio boutons pour le choix de cryptage ou décryptage
    choix = Radiobutton(root, text="Chiffrer", value=0, variable=valeurChoix)
    choix.grid(column=0, row=0)
    choix = Radiobutton(root, text="Déchiffrer", value=1, variable=valeurChoix)
    choix.grid(column=1, row=0)

    #Bouton de validation
    boutton_validation = Button(root, text="Valider", command = affichage)
    boutton_validation.grid(row=1, column=1)

    #Zone de saisie du texte à crypter ou décrypte
    zoneTexteSaisie = Entry(root, textvariable = phraseSaisie)
    zoneTexteSaisie.grid(column=0, row=1)
    zoneTexteSaisie.bind('<Button-1>', onClick)

    #Zone de saisie de la clé de cryptage
    zoneNbDecalage = Entry(root, textvariable = phraseNombre)
    zoneNbDecalage.grid(column=2, row=1, sticky='nsew')
    zoneNbDecalage.bind('<Button-1>', onClick2)

    #Zone d'indication pour l'affichage du résultat
    zoneTexteResultat = Label(root, text="Le texte correspondant :")
    zoneTexteResultat.grid(column=0 , row=2)

    #Zone d'affichage du résultat
    zoneAffichageResultat = Label(root)
    zoneAffichageResultat.grid(column=1 , row=2)

    #Case à cocher pour activer l'option circulaire ou non 
    checkCirculaire = Checkbutton(root, text=phraseChoixCirculaire.get(), variable=valeurChoixCirculaire)
    checkCirculaire.grid(column=2, row = 0, sticky='nsew')
    root.mainloop()
