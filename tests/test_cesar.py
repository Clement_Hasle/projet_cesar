import unittest
from cesar import chiffrage
from cesar import dechiffrage
from cesar import dechiffrageCirculaire
from cesar import chiffrageCirculaire

class TestCesar(unittest.TestCase):
    def testChiffrage(self):
        self.assertEqual(chiffrage("",8), "")
        self.assertEqual(chiffrage("GABET Benjamin",8), "OIJM\(Jmvriuqv")
        self.assertEqual(chiffrage("123",8), "9:;")
        self.assertEqual(chiffrage("",30), "")
        self.assertEqual(chiffrage("GABET Benjmin", 30), "e_`cr>`\x83\x8c\x88\x8b\x87\x8c")
        self.assertEqual(chiffrage("123", 30), "OPQ")

    def testDechiffrage(self):
        self.assertEqual(dechiffrage("",8), "")
        self.assertEqual(dechiffrage("OIJM\(Jmvriuqv",8), "GABET Benjamin")
        self.assertEqual(dechiffrage("9:;",8), "123")
        self.assertEqual(dechiffrage("",30),"")
        self.assertEqual(dechiffrage("e_`cr>`\x83\x8c\x88\x8b\x87\x8c",30), "GABET Benjmin")
        self.assertEqual(dechiffrage("OPQ", 30), "123")

    def testChiffrageCirculaire(self):
        self.assertEqual(chiffrageCirculaire("",8), "")
        self.assertEqual(chiffrageCirculaire("GABET Benjamin",8), "OIJMB Jmvriuqv")
        self.assertEqual(chiffrageCirculaire("123",8), "123")
        self.assertEqual(chiffrageCirculaire("",30), "")
        self.assertEqual(chiffrageCirculaire("GABET Benjamin", 30), "KEFIX Firneqmr")
        self.assertEqual(chiffrageCirculaire("123", 30), "123")
    
    def testDechiffrageCicurlaire(self):
        self.assertEqual(dechiffrageCirculaire("",8), "")
        self.assertEqual(dechiffrageCirculaire("OIJMB Jmvriuqv",8), "GABET Benjamin")
        self.assertEqual(dechiffrageCirculaire("9:;",8), "9:;")
        self.assertEqual(dechiffrageCirculaire("",30),"")
        self.assertEqual(dechiffrageCirculaire("KEFIX Firneqmr",30), "GABET Benjamin")
        self.assertEqual(dechiffrageCirculaire("123", 30), "123")